import React from 'react';
import './App.css';
import PostList from '../../components/PostList';
import PostDetail from '../../components/PostDetail';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
function App(){
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route exact path='/' render={() => <h2>Home page</h2>}/>
          <Route exact path='/post' component={PostList}></Route>
          <Route exact path='/post/:id' component={PostDetail}></Route>
          <Route exact path='/notfound' render={() => <h2>Page not found</h2>}></Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
}
export default App;
