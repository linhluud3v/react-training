import React from 'react';
import Song from '../Song'
import { getPosts, deletePost } from '../../request';
export default function ListSong (props) {
  const initData = [{id: 1, title: 'default'}, {id: 2, title: 'default1'}, {id: 3, title: 'default2'}];
  const [posts, setPost] = React.useState(initData);
  React.useEffect(() => {
    getPosts().then(result => {
      return setPost(result.data);
    });
  });
  
  return(
    <div>
      {posts.map(post => (
        <Song title={post.title} singler={post.body}></Song>
      ))}
    </div>
  )
}