import React from 'react';
import ButtonPrimary from '../ButtonPrimary';

export default function(props) {
  // debugger;
  return(
    <div className="post-item">
      <h3 className="post-title">{props.title}</h3>
      <p className="post-content">{props.body}</p>
      <ButtonPrimary/>
    </div>   
  )
}