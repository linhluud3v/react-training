import React, { useState } from 'react';
import { getPostsDetail } from '../../request';
import { Redirect } from 'react-router-dom';

export default function(props) {
  const initPost = {title: 'title', body: 'body'};
  var postId = props.match.params.id;
  const[ state, setState ] = useState(initPost);
  React.useEffect(() => {
    getPostsDetail(postId).then(post => {
      setState(post.data);
    })
  });
  
  if(postId === 'allibaba') {
    return(<Redirect to='/notfound'/>
    )
  }

  return(
    <div className="post-detail">
      <h3 className="post-header">{state.title}</h3>
      <p className="post-text">{state.body}</p>
    </div>   
  )
}