import React from 'react';
import './Song.css';
export default function Song(props) {
  return (
    <div className="song">
      <div className="song-info">
        <p className="song-title">{props.title}</p>
        <p className="song-singler">{props.body}</p>
      </div>
    </div>
  );
}

Song.defaultProps = {
  title: 'Default Title',
  body: 'Default text'
}

