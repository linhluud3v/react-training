import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import PostItem from '../PostItem';
import { getPosts } from '../../request/';

export default function PostList(props) {
  
  const initPost = [{title:"title", body:'body'}];
  const [ posts, setPost ] = React.useState(initPost);
  React.useEffect(() => {
    getPosts().then(post => {
      setPost(post.data);
    });
    // comment
  })
  return(
    <div>
      <h2> Post List </h2>
      <Container>
        <Row>
          {
            posts.map(({id, ...theRest})=> (
              <Col sm={4} md={4} key={id}>
                <PostItem {...theRest}/>
              </Col>
            ))
          }
        </Row>
      </Container>
    </div>
  )
}