import axios from 'axios';

const mainInstance = axios.create({
  baseURL: 'https://jsonplaceholder.typicode.com/',
  timeout: 1000,
  headers: {'X-Custom-Header': 'foobar'},
});

export function getPosts() {
  return mainInstance({
    url: '/posts',
    method: 'get'
  });
};

export function getPostsDetail(id) {
  return mainInstance({
    url: `/posts/${id}`,
    method: 'get'
  });
};

export function deletePost(postNumber) {
  // return axios.delete(`https://jsonplaceholder.typicode.com/posts/${postNumber}`)
  return mainInstance({
    url: `/posts/${postNumber}`,
    method: 'delete',
  });
}


